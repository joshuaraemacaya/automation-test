import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ChromeHeadlessTest {
	
	WebDriver driver;
	
	@Test
	public void verifyTitle() {
	System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
	ChromeOptions options=new ChromeOptions();
	options.addArguments("headless");
	options.addArguments("--remote-allow-origins=*");
	driver=new ChromeDriver(options);
	driver.get("https://www.browserstack.com/");
	System.out.println("Title is: " +driver.getTitle());
	Assert.assertEquals(driver.getTitle(), "Most Reliable App & Cross Browser Testing Platform | BrowserStack");
	
	driver.quit();
	}
}